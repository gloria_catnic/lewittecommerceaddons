<?php
/*
Plugin Name: Lewitt Ecommerce Addons 
Plugin URI: http://www.catnic.it
Description: Lewitt Ecommerce Addons 
Author: Catnic Srl
Version: 1.1
*/


class LewittEcommerceMenuWidget extends WP_Widget {

        //public function FocusBoxWidget() {
                // Istanzia l'oggetto genitore

                // WP_Widget::__construct( false, 'Lewitt Ecommerce Menu' );

        //}

         function __construct() {
            parent::__construct(false, 'Lewitt Ecommerce Menu', array( 'description' => 'Lewitt Ecommerce Menu' ) );
         }

        /**
         * Outputs the content of the widget
         *
         * @param array $args
         * @param array $instance
         */

        public function widget( $args, $instance, $widget_options = array(), $control_options = array() ) {
               extract($args);
               $out = $before_widget;
  
               global $post;
  
               $out .= '<ul id="ctn_menu_products">';
 
               $taxonomy     = 'product_cat';
               $orderby      = 'name';  
               $show_count   = 0;      // 1 for yes, 0 for no
               $pad_counts   = 0;      // 1 for yes, 0 for no
               $hierarchical = 1;      // 1 for yes, 0 for no  
               $title        = '';  
               $empty        = 0;
               $exclude      = '46';

               $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty,
                'exclude'       => $exclude
               );

               $categories = get_categories($args);

               foreach ($categories as $c) {

                 $name = $c->name;
                 $slug = $c->slug;                

                 $out .= '<li class="cat"><span>'.$name.'</span>';

                 $args = array(

                     'post_type' => 'product',
                     'post_status' => 'publish',
		     'posts_per_page' => -1, 
                     'tax_query' => array(

                       array(
                         'taxonomy' => 'product_cat',
                         'terms' => $slug,
                         'field' => 'slug'
                       )

                     )
                 );

                 $query = new WP_Query( $args );
                 $prods = $query->get_posts();

                 if (count($prods)) {

                    $found = false;

                    $ul_closed = '<ul class="closed">';
                    $ul_open = '<ul>';

                    $li = '';

                    foreach ($prods as $p) {
                       $pid = $p->ID;
                       if ($pid == $post->ID) {
                          $current = 'class="current"';
                       } else { 
                          $current = '';
                       }
 
                       $link = get_permalink($pid);
                       $li .= '<li '.$current.'><a href="'.$link.'">'.$p->post_title.'</a></li>';
                       if ($pid == $post->ID) {
                          $found = true;
                       }
                    }

                    if ($found) {
                      $out .= $ul_open; 
                    } else {
                      $out .= $ul_closed;
                    }

                    $out .= $li . '</ul>';

                 }

                 $out .= '</li>';

               }

               $out .= '</ul>';

               $out .= $after_widget;
               echo $out;
        }

}

function ctn_reg_widget() {
 register_widget('LewittEcommerceMenuWidget');
}

add_action( 'widgets_init', 'ctn_reg_widget');

function ctn_plugin_init() {
   $src = plugin_dir_url(__FILE__).'js/lewitt.js';
   wp_enqueue_script('lewitt', $src, array('jquery'));
}

add_action( 'init', 'ctn_plugin_init' );

